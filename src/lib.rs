pub fn quicksort(unsorted: Vec<i32>) -> Vec<i32> {
    // Base-case: Empty and one-item lists are already sorted
    if unsorted.len() <= 1 { return unsorted; }

    let pivot = unsorted[0];
    let mut less_than = Vec::new();
    let mut greater_than = Vec::new();

    for num in &unsorted[1..] {
        if *num <= pivot {
            less_than.push(*num);
        } else {
            greater_than.push(*num);
        }
    }

    let less_than = quicksort(less_than);
    let greater_than = quicksort(greater_than);

    // TODO Better way of concatenating
    let mut sorted = Vec::new();
    for num in less_than {
        sorted.push(num);
    }
    sorted.push(pivot);
    for num in greater_than {
        sorted.push(num);
    }

    sorted
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn quicksort_test() {
        assert_eq!(vec![1,1,2,2,2,2,3,3,4,5,8,8], quicksort(vec![5,2,3,2,8,1,4,2,3,2,8,1]))
    }
}