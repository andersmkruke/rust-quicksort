extern crate quicksort;

use quicksort::quicksort;

fn main() {
    // TODO take unsorted vector as input from user
    let unsorted = vec![1,5,4,7,6,2,4,4,5,3,7,9,10,4,13,1,0,-4,12,5];

    println!("Unsorted list: {:?}", unsorted);

    let sorted = quicksort(unsorted);

    println!("Sorted list: {:?}", sorted);
}